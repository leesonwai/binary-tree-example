# binary-tree-example

![License: GPL3](https://img.shields.io/badge/license-GPL3-green?style=flat-square)

This is a demonstration program written for my IT Professional Skills module.

## Building

```meson
meson builddir
cd builddir/
ninja
```
