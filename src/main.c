/* main.c
 *
 * Copyright 2019 Joshua Lee
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 32

typedef struct node
{
  char        *data;
  struct node *left;
  struct node *right;
} Node;

void
treefree (Node *n)
{
  if (n != NULL)
    {
      treefree (n->left);
      free (n->data);
      free (n);
      treefree (n->right);
    }
}

void
treeprint (Node *n)
{
  if (n != NULL)
    {
      treeprint (n->left);
      printf ("%s\n", n->data);
      treeprint (n->right);
    }
}

Node *
addtree (Node *n,
         char *w)
{
  int cond;

  if (!n)
    {
      n = malloc (sizeof (n));
      n->data = strdup (w);
      n->left = n->right = NULL;
    }
  else if ((cond = strcmp (w, n->data)) < 0)
    {
      n->left = addtree (n->left, w);
    }
  else if (cond > 0)
    {
      n->right = addtree (n->right, w);
    }

  return n;
}

int
getword (char *w,
         FILE *fp,
         int   lim)
{
  int c;

  while (isspace (c = getc (fp)) || ispunct (c))
    ;

  if (c == EOF)
    return EOF;

  if (isalnum (c))
    *w++ = tolower (c);

  while (!isspace (c = getc (fp)) && --lim)
    {
      if (isalnum (c))
        *w++ = tolower (c);
      else if (c == '\'' || c == '-')
        *w++ = c;
    }
  *w = '\0';

  return 0;
}

int
main (int   argc,
      char *argv[])
{
  FILE *fp;
  char w[MAX];
  Node *root = NULL;

  fp = fopen ("../data/keats.txt", "r");

  while (getword (w, fp, MAX) != EOF)
    {
      if (strlen (w))
        root = addtree (root, w);
    }
    
  treeprint (root);
  treefree (root);
  
  fclose (fp);

  return 0;
}
